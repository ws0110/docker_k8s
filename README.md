

## 환경 설정

### root 계정 설정
```
sudo passwd
su
```

#### 필요한 패키지 설치
```
yum install -y yum-utils device-mapper-persistent-data lvm2 wget unzip telnet
```

### docker-ce repo를 yum list 에 추가
```
# repo 추가
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# yum repolist 확인(docker-ce 확인)
yum repolist
```

### docker-ce 설치
```
yum install -y docker-ce
```

### docker 시작
```
# 시작
systemctl start docker && systemctl enable docker

# 확인
systemctl status docker
```

### docker 확인
docker info

---   
&nbsp;

## 실습 내용
* 다운로드경로: 좌측메뉴 'Downloads'로 이동하여 링크 주소 복사
```
mkdir /root/sample
cd /root/sample
wget [다운로드경로]
unzip *
```

---   
&nbsp; 

## Docker Hub 회원가입

1. https://hub.docker.com 접속
2. 왼쪽 상단 'Sign Up' 버튼 클릭
3. 내용을 작성하고 하단 'Sign Up' 클릭
4. 회원가입 시 입력했던 이메일로 접속하여 Confirm
5. DockerHub 접속 및 로그인 확인

---   
&nbsp;

## URL
## https://bitbucket.org/ws0110/docker_k8s
## https://cloud.google.com



