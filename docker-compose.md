

## Docker Compose

### Docker Compose 설치
```
curl -L https://github.com/docker/compose/releases/download/1.11.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

#### 실행 권한 부여
```
chmod +x /usr/local/bin/docker-compose
```

### 버전 확인
```
docker-compose -v
```

### docker-compose.yml 파일 작성 (3-Tier 기준: nginx/tomcat/mariadb)
- 첨부파일: docker-compose.yml

### YAML 파일 검증
```
docker-compose config
```

### docker-compose 실행
```
docker-compose up -d
```

