## Docker 리소스 모니터링
```
docker stats [컨테이너명 컨테이너명 ...] 
```
## Docker 컨테이너 접속 샘플
```
docker attach nginx
or
docker exec -it nginx bash[or sh]
```

## Docker Log Size limit 샘플
* Log Max 사이즈는 기본적으로 -1(unlimited) 임
```
# 최대 10메가 크기의 3개의 파일이 log rotate
docker run -d --log-opt max-size=10m --log-opt max-file=3 nginx
```

## Docker 메모리 제한 샘플
```
# 메모리 200M, 스왑 메모리 300M
docker run -d -m=200m --memory-swap=500m nginx
```

## Docker CPU 제한 샘플
```
# 50% CPU 사용
docker run -d --cpus=0.5 nginx
```

## 컨테이너 상세 정보 조회
```
docker inspect app
```

## Docker 환경정리
```
# 중지된 모든 컨테이너를 삭제
docker container prune

# 사용되지 않는 모든 이미지를 삭제
docker image prune

# 사용되지 않는 도커 네트워크를 모두 삭제
docker network prune

# 사용하지 않는 모든 도커 볼륨을 삭제
docker volume prune

# 사용하지 않는 모든 도커 리소스 전체삭제
docker system prune [-a]
```