## GKE Cluster 생성 (defaultpool : 3, 확장노드:5)



## 설치확인
```bash
# 버전 확인
$ kubectl version --short

# 쿠버네티스 핵심 컴포넌트 실행 확인
$ kubectl get pods --namespace kube-system

# Node 확인
$ kubectl get nodes

# Cluster 정보 확인
$ kubectl cluster-info

# 쿠버네티스에서 사용할 수 있는 오브젝트
$ kubectl api-resources
```

## 실습 환경설정
```bash
$ sudo passwd
$ su root
$ wget 다운로드경로
$ unzip 다운로드파일(zip)
$ chown -R 계정이름:계정이름 디렉토리이름
```


## Pod테스트
```bash
$ kubectl apply –f nginx-pod.yaml
```



## 3 Tier 테스트
```bash
# MariaDB Stateful 구성
$ kubectl apply –f mariadb-stateful.yaml

# MariaDB Service 구성
$ kubectl apply –f mariadb-service.yaml


# App Deployment 구성
$ kubectl apply –f app-deployment.yaml

# App Service 구성
$ kubectl apply –f app-service.yaml


# Web Deployment 구성
$ kubectl apply –f web-deployment.yaml

# Web Service 구성
$ kubectl apply –f web-service.yaml
```



## 모니터링
```bash
$ kubectl get deployments
$ kubectl get replicaset
$ kubectl get service
$ kubectl get pods –o wide
$ kubectl get all

# Pod상세 정보 확인
$ kubectl describe pods 파드이름

# Pod 로그 확인
$ kubectl logs –f 파드이름
```

---   

## URL
## https://cloud.google.com
## https://bitbucket.org/ws0110/k8s



