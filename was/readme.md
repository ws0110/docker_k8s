
# App 실행(base Local Image)

## Image Build
```
docker build -t app:1.0 .
```

## Image 확인
```
docker images
```

## 컨테이너 실행
```
docker run -d --name app -p 8080:8080 --link mariadb app:1.0
```

## 컨테이너 실행 확인
```
docker ps
```

## 컨테이너 로그 확인
```
docker logs -f app
```

## 접속 확인
* ip 변경
```
http://localhost:8080/
```

&nbsp;  
&nbsp;  

# App 실행(base DockerHub Image)

## Docker 로그인
```
docker login
```

## Image Tag
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker tag app:1.0 jangwisu/app:1.0
```

## Image Push
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker push jangwisu/app:1.0
```

## DockerHub 사이트 Push 이미지 확인

## 기존 app 컨테이너 Clear
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker stop app
docker rm app
docker rmi app:1.0
docker rmi jangwisu/app:1.0
```
## app 컨테이너 실행
* 이미지명 앞의 DockerHub 계정정보 변경
```
docker run -d --name app -p 8080:8080 --link mariadb jangwisu/app:1.0
```

## 컨테이너 및 이미지 확인
```
docker ps
docker images
docker logs -f app
```

## 접속 확인
* ip 변경
```
http://localhost:8080/
```